<%--
  Created by IntelliJ IDEA.
  User: ilona
  Date: 31.08.15
  Time: 23:52
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<c:url value="/resources/theme1/css/style.css" />" type="text/css" />
</head>
<body>
<c:import url="navigation.jsp" />
<h1>List  of costumers</h1>
    <table class="main_table">
        <tr class="navigation">
            <td>Id</td>
            <td>Title</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Type</td>
            <td></td>
            <td></td>
        </tr>

        <c:forEach items="${costumers}" var="element">
            <c:url var="deleteUrl" value="/delete?id=${element.id}" />
            <c:url var="editUrl" value="/edit?id=${element.id}" />
                <tr>
                    <td>${element.id}</td>
                    <td>${element.costumerTitle}</td>
                    <td>${element.firstName}</td>
                    <td>${element.lastName}</td>
                    <td>${element.type.costumerType}</td>
                    <td><a href="${editUrl}">Edit</a></td>
                    <td><a href="${deleteUrl}">Delete</a></td>
                </tr>
        </c:forEach>
    </table>
</body>
</html>
