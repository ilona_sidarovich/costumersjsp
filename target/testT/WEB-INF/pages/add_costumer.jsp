<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ilona
  Date: 03.08.15
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value="/resources/theme1/css/style.css" />" type="text/css" />
</head>
<body>

<c:import url="navigation.jsp" />
    <h1>Add costumer</h1>
<div class="login">
    <div class="login-card">
   <sf:form  method="POST" commandName="costumer" accept-charset="UTF-8">
            Title<br/>
            <sf:select path="costumerTitle" name="title">
                <sf:option value="MRS">Mrs</sf:option>
                <sf:option value="MR">Mr</sf:option>
                <sf:option value="MS">Ms</sf:option>
                <sf:option value="DR">Dr</sf:option>
            </sf:select>
            <br/>
            First Name<br/>
            <sf:input  path="firstName" name="firstName" type="text" required="true" pattern="^[a-zA-Z0-9]+$" title="Only english aphabet symbols!"/><br/>
            Last Name<br/>
            <sf:input  path="lastName" name="lastName" type="text" required="true" pattern="^[a-zA-Z0-9]+$" title="Only english aphabet symbols!"/><br/>
            Type<br/>
            <sf:select path="type.costumerType" name="type">
                <sf:option value="RESIDENTAL">Residental</sf:option>
                <sf:option value="SMALL_MEDIUM_BUSINESS">Small/Medium Business</sf:option>
                <sf:option value="ENTERPRISE">Enterprise</sf:option>
            </sf:select>
            </br></br>
            <sf:input path="" type="submit" name="submit_button" value="Confirm"/>
        </sf:form>
       </div>
    </div>
<br/>
</body>
</html>
