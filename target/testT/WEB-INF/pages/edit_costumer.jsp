<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: ilona
  Date: 03.09.15
  Time: 0:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<c:url value="/resources/theme1/css/style.css" />" type="text/css" />
</head>
<body>
<c:url var="saveUrl" value="edit?id=${costumerAttribute.id}" />
<c:import url="navigation.jsp" />
<h1>Edit costumer # ${costumerAttribute.id}</h1></br>
<div class="login">
    <div class="login-card">
    <sf:form  modelAttribute="costumerAttribute" method="POST" action="${saveUrl}" accept-charset="UTF-8">Title<br/>
        <sf:select path="costumerTitle" name="title">
            <sf:option value="MRS">Mrs</sf:option>
            <sf:option value="MR">Mr</sf:option>
            <sf:option value="MS">Ms</sf:option>
            <sf:option value="DR">Dr</sf:option>
        </sf:select>
        <br/>
        First Name<br/>
        <sf:input  path="firstName" name="firstName" type="text"/><br/>
        Last Name<br/>
        <sf:input  path="lastName" name="lastName" type="text"/><br/>
        Type<br/>
        <sf:select path="type.costumerType" name="type">
            <sf:option value="RESIDENTAL">Residental</sf:option>
            <sf:option value="SMALL_MEDIUM_BUSINESS">Small/Medium Business</sf:option>
            <sf:option value="ENTERPRISE">Enterprise</sf:option>
        </sf:select>
        </br></br>
        <sf:input path="" type="submit" name="button" value="Save changes" cssClass="button"/>
    </sf:form>
    </div>
</div>
</body>
</html>
