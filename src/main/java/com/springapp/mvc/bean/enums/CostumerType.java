package com.springapp.mvc.bean.enums;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by ilona on 01.09.15.
 */

public enum CostumerType {
    RESIDENTIAL, SMALL_MEDIUM_BUSINESS, ENTERPRISE
}
