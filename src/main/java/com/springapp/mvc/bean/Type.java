package com.springapp.mvc.bean;

import com.springapp.mvc.bean.enums.CostumerType;
import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * Created by ilona on 01.09.15.
 */
@Component
@Entity
@Table(name = ("TYPE"))
public class Type {
    @Id
    @Column(name = ("TYPE_ID"))
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(name = ("COSTUMER_TYPE"))
    private String costumerType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCostumerType() {
        return costumerType;
    }

    public void setCostumerType(String costumerType) {
        this.costumerType = costumerType;
    }
}
