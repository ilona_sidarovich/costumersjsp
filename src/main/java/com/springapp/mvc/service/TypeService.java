package com.springapp.mvc.service;

import com.springapp.mvc.bean.Type;
import com.springapp.mvc.dao.impl.TypeDaoImpl;
import com.springapp.mvc.exception.DAOException;
import com.springapp.mvc.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* Created by ilona on 28.08.15.
*/
@Service
public class TypeService {
    @Autowired
    TypeDaoImpl typeDao;
    public void createType(Type type) throws ServiceException {
        try {
            typeDao.createType(type);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }
}
