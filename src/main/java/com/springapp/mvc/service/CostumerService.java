package com.springapp.mvc.service;


import com.springapp.mvc.bean.Costumer;
import com.springapp.mvc.dao.impl.CostumerDaoImpl;
import com.springapp.mvc.exception.DAOException;
import com.springapp.mvc.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ilona on 28.08.15.
 */
@Service
public class CostumerService {
    @Autowired
    CostumerDaoImpl costumerDao;

    public void createCostumer(Costumer costumer) throws ServiceException {
        try {
            costumerDao.createCostumer(costumer);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    public List<Costumer> viewAllCostumers() throws ServiceException {
        List<Costumer> costumers = new ArrayList<Costumer>();
        try {
            costumers = costumerDao.findAll();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return costumers;
    }
    public void deleteCostumer(Long costumer) throws ServiceException {
        try {
            costumerDao.deleteCostumer(costumer);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    public Costumer getCostumerById(Long id) throws ServiceException{
        Costumer costumer = new Costumer();
        try {
            costumer=costumerDao.findCostumerById(id);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return costumer;
    }

    public void updateCostumer (Costumer costumer) throws ServiceException {
        try {
            costumerDao.update(costumer);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    public List<Costumer> findCostumerByFirstLastNameMethaphone(String firstName, String lastName) throws ServiceException{
        List<Costumer> costumers;
        try {
            costumers=costumerDao.findCostumerByFirstLastNameMethaphone(firstName, lastName);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return costumers;
    }
}
