package com.springapp.mvc.controller;



import com.springapp.mvc.bean.Costumer;
import com.springapp.mvc.bean.Type;
import com.springapp.mvc.exception.ServiceException;
import com.springapp.mvc.service.CostumerService;
import com.springapp.mvc.service.TypeService;
import org.apache.commons.codec.language.Metaphone;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilona on 03.08.15.
 */
@Controller

public class CostumerController {
    private static final Logger LOG = Logger.getLogger(CostumerController.class);
    @Autowired
    CostumerService costumerService;
    @Autowired
    TypeService typeService;


    @RequestMapping(value = "/")
    public ModelAndView main (ServletRequest request){
        ModelAndView mv = new ModelAndView("costumers_list");
        List<Costumer> costumers = null;
        try {
            costumers = costumerService.viewAllCostumers();
            LOG.info(request.getRemoteAddr());
        } catch (ServiceException e) {
            LOG.error(e);
        }

        mv.addObject("costumers", costumers);
        return mv;
    }

    @RequestMapping(value = "/add_costumer", method = RequestMethod.GET)
    public ModelAndView goToAddPage (){
        ModelAndView mv = new ModelAndView("add_costumer");
        mv.addObject("costumer", new Costumer());
        mv.addObject("type", new Type());
        return mv;
    }

    @RequestMapping( value="/add_costumer", method = RequestMethod.POST)
    public ModelAndView addUser ( @ModelAttribute  ("costumer") Costumer costumer, @ModelAttribute  ("type") Type type,ServletRequest request ){
            ModelAndView mv = new ModelAndView("costumers_list");
            mv.addObject("costumer", costumer);
            mv.addObject("type", type);
        costumer.setFirstNameMetaphone(new Metaphone().metaphone(costumer.getFirstName()));
        costumer.setLastNameMetaphone(new Metaphone().metaphone(costumer.getLastName()));
        type.setCostumerType(costumer.getType().getCostumerType());
        costumer.setType(type);
        try {
            typeService.createType(type);
            LOG.info("type " + type.getId() + " is created " + request.getRemoteAddr());
            costumerService.createCostumer(costumer);
            LOG.info("costumer " + costumer.getId() +" is created " +request.getRemoteAddr());
        } catch (ServiceException e) {
            LOG.error(e);
        }
        List<Costumer> costumers = null;
        try {
            costumers = costumerService.viewAllCostumers();
        } catch (ServiceException e) {
            LOG.error(e);
        }
        mv.addObject("costumers", costumers);
        return mv;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value="id", required=true) Long id, Model model, ServletRequest request) {
        try {
            costumerService.deleteCostumer(id);
            LOG.info("costumer " + id +" is deleted " + request.getRemoteAddr());
        } catch (ServiceException e) {
            LOG.error(e);
        }
        model.addAttribute("id", id);
        List<Costumer> costumers = null;
        try {
            costumers = costumerService.viewAllCostumers();
            LOG.info(request.getRemoteAddr());
        } catch (ServiceException e) {
            LOG.error(e);
        }
        model.addAttribute("costumers", costumers);

        return "costumers_list";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String getEdit(@RequestParam(value="id", required=true) Long id, Model model, ServletRequest request) {
        LOG.debug("Received request to show edit page");
        Costumer costumer = new Costumer();
        try {
            costumer=costumerService.getCostumerById(id);
            LOG.info(request.getRemoteAddr());
        } catch (ServiceException e) {
            LOG.error(e);
        }
        if(costumer!=null){
        model.addAttribute("costumerAttribute", costumer);
        return "edit_costumer";}
        else {
            return "error";
        }
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String saveEdit(@ModelAttribute("costumerAttribute") Costumer costumer, @RequestParam(value="id", required=true) Long id, Model model, ServletRequest request) {
        LOG.debug("Received request to update person");
        costumer.setId(id);
        try {
            costumerService.updateCostumer(costumer);
            LOG.info("costumer " + costumer.getId() + " updated " + request.getRemoteAddr());
        } catch (ServiceException e) {
            LOG.error(e);
        }
        model.addAttribute("id", id);
        List<Costumer> costumers = null;
        try {
            costumers = costumerService.viewAllCostumers();
        } catch (ServiceException e) {
            LOG.error(e);
        }
        model.addAttribute("costumers", costumers);
        return "costumers_list";
    }


    @RequestMapping(value="/find_costumer_page", method = RequestMethod.GET)
    public ModelAndView findCostumer(){
        ModelAndView mv = new ModelAndView("find_costumer");
        mv.addObject("costumer", new Costumer());
        return mv;
    }

    @RequestMapping( value="/find_costumer_page", method = RequestMethod.POST)
    public ModelAndView findCostumerPost ( @ModelAttribute  ("costumer") Costumer costumer){
        ModelAndView mv = new ModelAndView("costumers_list");
        mv.addObject("costumer", costumer);
        String firstName = costumer.getFirstName();
        String lastName = costumer.getLastName();
        List<Costumer> costumers =null;
        try {
            costumers = costumerService.findCostumerByFirstLastNameMethaphone(firstName, lastName);
        } catch (ServiceException e) {
            LOG.error(e);
        }
        mv.addObject("costumers", costumers);
        return mv;
    }

}
