package com.springapp.mvc.dao;

import com.springapp.mvc.bean.Type;
import com.springapp.mvc.exception.DAOException;

/**
* Created by ilona on 28.08.15.
*/
public interface TypeDao {
    public void createType(Type type) throws DAOException;
}
