package com.springapp.mvc.dao;



import com.springapp.mvc.bean.Costumer;
import com.springapp.mvc.exception.DAOException;

import java.util.List;

/**
 * Created by ilona on 28.08.15.
 */
public interface CostumerDao {
    public void createCostumer(Costumer costumer) throws DAOException;
    public List<Costumer> findAll() throws DAOException;
    public void update(Costumer costumer) throws DAOException;
    public void deleteCostumer(Long id) throws DAOException;
    public Costumer findCostumerById(Long id) throws DAOException;
}
