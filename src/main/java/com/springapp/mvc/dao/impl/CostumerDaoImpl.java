package com.springapp.mvc.dao.impl;


import com.springapp.mvc.bean.Costumer;
import com.springapp.mvc.bean.enums.CostumerTitle;
import com.springapp.mvc.bean.enums.CostumerType;
import com.springapp.mvc.dao.CostumerDao;
import com.springapp.mvc.exception.DAOException;
import com.springapp.mvc.util.HibernateUtil;
import org.apache.commons.codec.language.Metaphone;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by ilona on 28.08.15.
 */
@Repository
public class CostumerDaoImpl implements CostumerDao {
    private static final Logger LOG = Logger.getLogger(CostumerDaoImpl.class);
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public void createCostumer(Costumer costumer) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        costumer.setModifiedWhen(new Date());
        try {
            session.beginTransaction();
            if(costumer!=null){
            session.save(costumer);
            }
            session.getTransaction().commit();
            LOG.info(costumer.getFirstName() + " " + costumer.getLastName()+ " has been created");
        } catch (HibernateException e) {
            LOG.error(e);
            session.getTransaction().rollback();
        }
    }

    @Override
    public List<Costumer> findAll() throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Costumer> costumers = session.createCriteria(Costumer.class)
                .addOrder(Order.desc("modifiedWhen"))
                .setMaxResults(10)
                .list();
        session.getTransaction().commit();
        return costumers;
    }

    @Override
    public void update(Costumer costumer) throws DAOException {

        LOG.debug("Editing existing person");

        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        Costumer existingCostumer = (Costumer) session.get(Costumer.class, costumer.getId());
        existingCostumer.setFirstName(costumer.getFirstName());
        existingCostumer.setLastName(costumer.getLastName());
        existingCostumer.setType(existingCostumer.getType());
        existingCostumer.setModifiedWhen(new Date());
        try {
            session.save(existingCostumer);
            session.getTransaction().commit();
            LOG.info(costumer.getFirstName() + " " + costumer.getLastName()+ " has been updated");

        } catch (HibernateException e) {
            LOG.error(e);
            session.getTransaction().rollback();
        }
    }

    @Override
    public void deleteCostumer(Long id) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            Costumer costumer = (Costumer) session.get(Costumer.class, id);
            if(costumer!=null){
            session.delete(costumer);
            }
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            LOG.error(e);
            session.getTransaction().rollback();
        }
    }

    @Override
    public Costumer findCostumerById(Long id) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        Costumer costumer = (Costumer) session.get(Costumer.class, id);
        session.getTransaction().commit();
        return costumer;
    }

    public List<Costumer> findCostumerByFirstLastNameMethaphone(String firstName, String lastName) throws DAOException{
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Costumer> costumers =
                session.createCriteria(Costumer.class)
                        .add(Restrictions.like("firstNameMetaphone", new Metaphone().metaphone(firstName)))
                        .add(Restrictions.like("lastNameMetaphone", new Metaphone().metaphone(lastName)))
                        .list();
        session.getTransaction().commit();
        return costumers;
    }
}
