package com.springapp.mvc.dao.impl;

import com.springapp.mvc.bean.Type;
import com.springapp.mvc.dao.TypeDao;
import com.springapp.mvc.exception.DAOException;
import com.springapp.mvc.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
* Created by ilona on 28.08.15.
*/
@Repository
public class TypeDaoImpl implements TypeDao{
    private static final Logger LOG = Logger.getLogger(TypeDaoImpl.class);
    @Override
    public void createType(Type type) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            if(type!=null){
            session.save(type);
            }
            session.getTransaction().commit();
        } catch (HibernateException e) {
            LOG.error(e);
            session.getTransaction().rollback();
        }
    }
}
