<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ilona
  Date: 04.09.15
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link rel="stylesheet" href="<c:url value="/resources/theme1/css/style.css" />" type="text/css" />
</head>
<body>
<c:import url="navigation.jsp" />
<h1>Find Costumer</h1>
<div class="login">
    <div class="login-card">
    <sf:form  modelAttribute="costumer" method="POST" action="find_costumer_page" accept-charset="UTF-8">
        <br/>
    First Name<br/>
<sf:input  path="firstName" name="firstName"  type="text" required="true" pattern="^[a-zA-Z0-9]+$" title="Only english aphabet symbols!" /><br/>
    Last Name<br/>
    <sf:input  path="lastName" name="lastName" type="text" required="true" pattern="^[a-zA-Z0-9]+$" title="Only english aphabet symbols!"/><br/>
    </br>
    <sf:input path="" type="submit" name="submit_button" value="Find Costumer" />
</sf:form>
        </div>
    </div>
</body>
</html>
